package main.java;

import main.java.services.FileService;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Program{
    public static void main(String[] args) {
        menu();
    }

    private static void menu(){
        Scanner scanner = new Scanner(System.in);
        FileService fileService = new FileService();
        String userChoise = "";
        
        // Provide users with options until quitting.
        while(!(userChoise == "4")){
            System.out.println("************** MENU *************");
            System.out.print("Choose an option:\n1) Get filenames\n2) Get filenames by extension\n3) Manipulate file\n4) Exit\n");
            System.out.print("*********************************\n> ");
            try {
                userChoise = scanner.nextLine();
            } catch (InputMismatchException e) {
                System.err.println(e);
                menu();
            } 
            catch(NoSuchElementException e){
                System.err.println(e);

            }

            // Calling the desired function for file manipulation.
            switch(userChoise){
                case "1": // List files in given directory.
                    clearConsole();
                    fileService.printFilenames();
                    break;
                case "2": // List files with a specific extension.
                    clearConsole();
                    System.out.print("\nType extension (.txt, .png etc..)\n> ");
                    String extension = null;
                    try {
                        extension = scanner.nextLine();
                    } catch (Exception e) {
                        clearConsole();
                        System.err.println(e);
                        menu();
                    }
                    if(!extension.equals("")){
                        if(String.valueOf(extension.charAt(0)).equals(".")){
                            extension = extension.substring(1); // Remove . for later comparison.
                        }
                        fileService.printFilenames(extension);
                    }
                    break;
                case "3": // Manipulate files.
                    clearConsole();
                    System.out.print("Write word you want to search for:\n> ");

                    String word = null;
                    try {
                        word = scanner.nextLine().toLowerCase(); // ignore lowercase.
                    } catch (Exception e) {
                        clearConsole();
                        System.err.println(e);
                        menu();
                    }
                    fileService.manipulateFile(word);
                    break;
                case "4": // Exit menu loop.
                clearConsole();
                    System.out.println("\nSystem exit.");
                    System.exit(0);
                    break;
            
                default:
                    clearConsole();
                    System.out.println("\n\nPlease make a valid option.\n");
                    menu();
                    break;
            }
        }
        scanner.close();  
    }
    public static void clearConsole(){
        // Print several newlines to clear console.
        System.out.println(System.lineSeparator().repeat(50));
    }
}