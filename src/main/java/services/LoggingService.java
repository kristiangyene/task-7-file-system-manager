package main.java.services;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LoggingService {

   public LoggingService(){
       try {
           FileWriter fileWriter = new FileWriter("../src/main/resources/log.txt");
           fileWriter.flush(); // Remove all in file when program starts.
           fileWriter.close();
       } catch (IOException e) {
           System.err.println(e);
       }
   }

   public void log(String message, long time){
       // Log message and time gotten from function call.
       try {
           FileWriter fileWriter = new FileWriter("../src/main/resources/log.txt", true);
           DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
           fileWriter.write("-------------------------------------------------\n");
           fileWriter.write(LocalDateTime.now().format(formatter) + "\n");
           fileWriter.write("\n" + message);
           fileWriter.write("Function time: " + time + "ms\n");
           fileWriter.write("-------------------------------------------------\n");
           fileWriter.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}