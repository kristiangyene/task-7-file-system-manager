package main.java.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class FileService {
    File folder = null;
    LoggingService loggingService = null;

    public FileService(){
        loggingService = new LoggingService();
        try {
            this.folder = new File("../src/main/resources/Files");
        } catch (Exception e) {
            System.err.println("\n\n" + e + "\n");
        }
    }


    public void printFilenames(){
        long start = System.currentTimeMillis();
        String filenames = "";
        // Loop through files in folder.
        for(File file: this.folder.listFiles()){
            filenames += (file.getName() + "\n");
        }
        filenames += "\n";
        System.out.println(filenames);

        long end = System.currentTimeMillis() - start;
        loggingService.log("File list:\n" + filenames, end);
    }


    public void printFilenames(String extension){
        long start = System.currentTimeMillis();
        String filenames = "";
        // Lopp through files in folder.
        for(File file: this.folder.listFiles()){
            String filename = file.getName();
            String fileExtension = filename.split("\\.", 2)[1];
            // Compare fileextension with user input.
            if(fileExtension.equals(extension)){
                filenames += (file.getName() + "\n");
            }
        }
        filenames += "\n";
        System.out.println(filenames);

        long end = System.currentTimeMillis() - start;
        loggingService.log("Files with the extension '" + extension + "':\n" + filenames, end);
    }


    public void manipulateFile(String word){
        long start = System.currentTimeMillis();
    
        File file = new File(folder.toPath() + "/Dracula.txt");
        String info = "";
        
        // First three requirements.
        info += ("File name: " + file.getName() + "\n");
        info += ("File size: " + (file.length()/1024) + "kB\n");
        info += ("Number of lines: " + getNumberOfLines(file) + "\n");

        System.out.println(info);

        int wordCount = 0;

        try { // Count the given word from user.
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNext()){ //Check for every word.
                String next = fileScanner.next();
                if(next.toLowerCase().equals(word)) wordCount++;
                
            }
            fileScanner.close();
        } catch (Exception e) {
            System.err.println(e);
        }

        String wordString = "The word '" + word + "' was found " + wordCount + " times.\n\n";
        System.out.println(wordString);

        long end = System.currentTimeMillis() - start;
        info += wordString;
        loggingService.log(info, end);
    }


    public int getNumberOfLines(File file){
        int lines = 1;
        try {
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNextLine()){
                fileScanner.nextLine();
                lines++;
            }
            fileScanner.close();
        } catch (FileNotFoundException e) {
            System.err.println(e);
        }
        return lines;
    }

}
