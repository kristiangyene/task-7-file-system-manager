# Task 7 - File System Manager

## About project

The system provides the user with three functionalities:
- List of filenames in a given directory.
- List of filenames in the directory of a user given extension.
- Displays the name, file size and number of lines for the file Dracula.txt. The user can also search in the file for a specific word.

Every result, timestamp and runtime for the functionalities will be logged in a file. 

### Note 

Every command run shall be executed in the 'out' directory. 

### Compile files

```
$ javac -d out ../src/main/java/Program.java ../src/main/java/services/*.java
```

### Create .jar file

```
$ jar cfe Program.jar Program main/java/Program.class main/java/services/*.class  
```

### Run .jar file

```
$ java -jar Program.jar

************** MENU *************
Choose an option:
1) Get filenames
2) Get filenames by extension
3) Manipulate file
4) Exit
*********************************
>                                 
```

**I had problems taking screenshots with my computer so no image today...**